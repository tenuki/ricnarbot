This is the FrasesNarvajianas Bot!

Use responsibly!


Sample configuration should be save at ~/botconfig.ini :


[botcfg]
filename=~/Bot.cfg
welcome=Bienvenido a @...Bot!

[filebackend]
filename=~/frases.txt

[people]
fulano=500..234
sutano=13...345

[access]
add=fulano,sutano
quit=sutano
