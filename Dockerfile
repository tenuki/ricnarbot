FROM python:3-alpine
WORKDIR /code
RUN apk add --no-cache gcc musl-dev linux-headers libffi-dev openssl-dev
COPY src .
RUN pip install python-telegram-bot ConfigObject 
CMD ["python", "bot.py"]
