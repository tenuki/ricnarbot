import logging
import random
import sys
from string import ascii_letters as ASCII
from string import digits
from filterstr import prepare_to_filter

logger = logging.getLogger("backend")

# try:
#     from hot_redis import Set as RSet
#     from hot_redis import List as RList
#     HAVE_REDIS = True
# except:
#    HAVE_REDIS = False
HAVE_REDIS = False


J = lambda x: ''.join(x)


def read(filename):
    with open(filename, 'rb') as f:
        return f.read().decode("utf-8").strip()


def ufilter(a_str):
    AD = set(ASCII + digits)
    filtr = prepare_to_filter(a_str)
    return J([c if f in AD else ' '
              for c, f in zip(a_str, filtr)])


class Backend:
    FREQ_MAX = 30

    def __init__(self):
        self.freq_map = {}
        self.word_fr = {}

    @staticmethod
    def get_words(frase):
        frase = ufilter(frase)
        for word in frase.split(' '):
            if len(word) < 2:
                continue
            yield word.lower()

    def update_freq(self):
        fm = {}
        _all = self.get_all()
        for frase in _all:
            frase = ufilter(frase)
            for pal in self.get_words(frase):
                fm[pal] = x = fm.setdefault(pal, 0) + 1
        self.word_fr = fm
        return fm

    def frase_score(self, frase):
        _min = self.FREQ_MAX
        _wrd = None

        for word in self.get_words(frase):
            scr = self.word_fr.get(word, None)
            logger.warning("word: %s  ->  %r" % (word, scr))
            if scr is None:
                continue
            if scr < _min:
                _min = scr
                _wrd = word
        return _wrd

    def cantidad(self):
        raise NotImplementedError()

    def get_all(self):
        raise NotImplementedError()

    def answer(self, word):
        try:
            return random.choice(self.get_for(word))
        except IndexError:
            return ':-('

    def get_for(self, word):
        if len(word) < 3:
            return []
        return self._get_for(word)

    def _get_for(self, word):
        word_l = word.lower()
        return [frase for frase in self.get_all()
                if self.match(frase, word_l, True)]

    def match(self, frase, word, already_lower=False):
        word_l = word if already_lower else word.lower()
        return word_l in frase.lower()


class FileBackend(Backend):
    def __init__(self, filename):
        Backend.__init__(self)
        self.Frases = {}
        raw_frases = read(filename).splitlines()
        for frase in raw_frases:
            print(repr(frase), type(frase))
            filtered = prepare_to_filter(frase)
            self.Frases[filtered] = frase
        if len(self.Frases) == 0:
            print('no fraaes foune.')
            sys.exit(-1)
        self.update_freq()

    def cantidad(self):
        return len(self.Frases)

    def get_all(self):
        return self.Frases.values()

    def _get_for(self, word):
        word_l = prepare_to_filter(word)
        return [frase for filtered, frase in self.Frases.items()
                if word_l in filtered]

    def match(self, frase, word, already_lower=False):
        word_l = word if already_lower else prepare_to_filter(word)
        return word_l in prepare_to_filter(frase)

    # def add(self, frase):
    #    self.Frases.append(frase)

# if HAVE_REDIS:
#  class RedisBackend(Backend):
#     def __init__(self, dbidx):
#         self.frases = RSet(key='frases')
#
#     def get_all(self):
#         return self.frases
#
#     def cantidad(self):
#         return len(self.frases)
#
#     def add(self, frase):
#         self.frases.add(frase)
