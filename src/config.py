from ConfigObject import ConfigObject

class Config:
    def __init__(self, filename):
        self.cfg = ConfigObject(filename = filename)
        self.check()

    def check(self):
        if self.cfg.redis.db:
            print(self.cfg.redis.db)


    def save(self):
        self.cfg.write()
