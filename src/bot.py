import logging
import os
import re
import threading
import time
import traceback
from queue import Queue
from uuid import uuid4

from ConfigObject import ConfigObject
from telegram import InlineQueryResultArticle, InputTextMessageContent
from telegram.ext import InlineQueryHandler
from telegram.ext import Updater, MessageHandler, Filters, CommandHandler

import access
from backend import FileBackend

try:
    from backend import RedisBackend
except:
    pass

logging.basicConfig(format='%(name)s %(levelname)s - %(message)s',
                    level=logging.WARNING)
logger = logging.getLogger(__name__)

XU = os.path.expanduser
Config = ConfigObject(filename=XU("~/botconfig.ini"))

Backend = FileBackend(XU(os.environ["FRASESFILE"]))
Uptime = time.time()
updater = Updater(token=os.environ["TOKEN"], use_context=True)
dispatcher = updater.dispatcher

#Access = access.Access(Config)

# configuration
CACHE_TIME = 5 * 60  # seconds  ##CACHE_TIME = 5
ANSWER_CHUNK_LEN = 50  # for telegram as of aug/2017

# our globals
QUERY_LOCK = threading.Lock()
QUERIES = {}
QUERY_Q = Queue()


def escape_markdown(text):
    escape_chars = '\*_`\['
    return re.sub(r'([%s])' % escape_chars, r'\\\1', text)


def queue_cleanup_thread():
    while True:
        query = QUERY_Q.get()
        time.sleep(5 * 60)
        with QUERY_LOCK:
            try:
                del QUERIES[query]
            except:
                pass


def inline_query(update, context):
    try:
        query = update.inline_query
        with QUERY_LOCK:
            full = results = QUERIES.get(query.id)

        if results is None:
            results = list()
            frases = Backend.get_for(query.query)
            if len(frases) == 0:
                frases.append(":-( - 0 frases / palabra muy corta")
            for idx, frase in enumerate(frases):
                uu = uuid4()
                title = frase  # '%d: %s'%(idx, frase)
                # title = title if len(title)<47 else title[:45]+'..'
                results.append(
                    InlineQueryResultArticle(
                        id=uu, title=title,
                        input_message_content=InputTextMessageContent(frase)
                    )
                )
            with QUERY_LOCK:
                QUERIES[query.id] = full = results
            QUERY_Q.put(query.id)

        try:
            base = int(query.offset)
        except Exception:
            base = 0

        total = len(full)
        results = full[base:base + ANSWER_CHUNK_LEN]
        more = total > base + ANSWER_CHUNK_LEN
        logger.warning("%s %s results size: %d base: %d more: %r" % (
            query.id, query.query, len(full), base, more))

        Retry = 2
        while Retry > 0:
            try:
                if more:
                    update.inline_query.answer(results,
                                               cache_time=CACHE_TIME,
                                               next_offset=str(
                                                   base + ANSWER_CHUNK_LEN))
                else:
                    update.inline_query.answer(results,
                                               cache_time=CACHE_TIME)
                return
            except Exception as e:
                print('exception -> ', e)
                Retry -= 1
                time.sleep(0.3)
        #            id=uuid4(), title="Bold",
        #            input_message_content=InputTextMessageContent(
        #                "*%s*" % escape_markdown(query), parse_mode=ParseMode.MARKDOWN
        #            id=uuid4(), title="Italic",
        #            input_message_content=InputTextMessageContent(
        #                "_%s_" % escape_markdown(query), parse_mode=ParseMode.MARKDOWN
    except:
        traceback.print_exc()


def get(_parts, item):
    parts = _parts[1:] if item == 0 else _parts[:-1]
    return ' '.join(parts), int(_parts[item])


def start(bot, update):
    update.message.reply_text(Config.botcfg.welcome)


def stats(bot, update):
    try:
        update.message.reply_text(
            '\r\n'.join([
                'Por ahora solo %d frases!' % Backend.cantidad(),
                'Funcionando ininterrumpidamente desde hace %.02f segundos' % (
                            time.time() - Uptime)
            ]))
    except:
        traceback.print_exc()


# def quit(bot, update):
#     if Access.can_quit(update.message.chat_id):
#         dispatcher.stop()


# def add(bot, update):
#     if Access.can_add(update.message.chat_id):
#         msg = update.message.text.split(' ', 1)[1]
#         Backend.add(msg)
#         response = 'thank you!'
#         # print dir(update)
#         # print dir(update.message)
#         # print
#     else:
#         response = 'be back later..'
#     update.message.reply_text(response)


LAST_CHATS = {}


def echo(update, context):
    """Echo the user message."""
    msg = update.message.text
    parts = msg.split(' ')
    parte = parts[0].lower()
    newmsg = Backend.answer(parte)
    update.message.reply_text(newmsg)


# def answer_to_user(bot, update):
#     try:
#         msg = update.message.text
#         print(update.message.from_user)
#         print(update.message.chat_id, '<--', msg)
#         parts = msg.split(' ')
#         parte = parts[0].lower()
#         newmsg = Backend.answer(parte)
#         bot.send_message(chat_id=update.message.chat_id, text=newmsg)
#     except:
#         traceback.print_exc()
#
#
# def answer_to_group(bot, update):
#     chat = update.message.chat_id
#     now = time.time()
#     prev = LAST_CHATS.get(chat, 0)
#     if now - prev < 3 * 60:
#         # too fast.. :-(
#         return
#     try:
#         msg = update.message.text
#         best = Backend.frase_score(msg)
#         logger.warning("selected: %s" % best)
#         if best is None:
#             return
#         newmsg = Backend.answer(best)
#         bot.send_message(chat_id=chat, text=newmsg)
#         LAST_CHATS[chat] = now
#     except:
#         traceback.print_exc()


def error(bot, update, error):
    logger.warning("Update: %s caused: %s" % (update, error))


t = threading.Thread(target=queue_cleanup_thread)
t.start()

dispatcher.add_handler(CommandHandler("start", start))
dispatcher.add_handler(CommandHandler("stats", stats))
# dispatcher.add_handler(CommandHandler("add", add))
# dispatcher.add_handler(CommandHandler("quit", quit))
dispatcher.add_handler(InlineQueryHandler(inline_query))
dispatcher.add_handler(MessageHandler(Filters.text, echo))
try:
    updater.start_polling()
    updater.idle()
except:
    updater.stop()
