from unicodedata import normalize
_normalize_cache = {}


def _search_normalizer(char):
    """Normalize always to one char length."""
    try:
        return _normalize_cache[char]
    except KeyError:
        norm = normalize('NFKD', char).encode('ASCII', 'ignore').lower()
        if not norm:
            norm = '?'
        _normalize_cache[char] = norm
        return norm


def prepare_to_filter(text):
    """Prepare a text to filter.
    It receives unicode, but return simple lowercase ascii.
    """
    return ''.join(_search_normalizer(c).decode("ascii") for c in text)
